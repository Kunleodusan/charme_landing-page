<!DOCTYPE html>
<html>
  <head>
    <title>Charmé</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>  
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    
    
    <!-- Included Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lora:400,900,700,500,300,100' rel='stylesheet' type='text/css'>
    
    
    <!-- CSS Files comes here --><!-- 
    <link href="{{asset('css/main.css')}}" rel="stylesheet" media="screen">         -->                           <!-- Main CSS file -->
    <link href="{{asset('css/colors/color_palette_charme.css')}}" rel="stylesheet" media="screen">              <!-- Color palette -->
    <link href="{{asset('css/rapid-icons.css')}}" rel="stylesheet" media="screen">                            <!-- James Framework - ICONS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
 </head>
  
  <body>
    <nav class="navbar">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="{{url('/')}}">              
            <img src="{{asset('images/charme_pink.png')}}" alt="logo"/>
          </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
          </ul>
        </div>
      </div>
    </nav>
    
        <!-- Fullscreen homepage -->
        <section class="hero_fullscreen background_slider menu_bar-waypoint" data-animate-down="menu_bar-hide" data-animate-up="menu_bar-hide" >
            <!-- This section class is where you can edit background choice (background_single, background_slider, background_video) you can also enable gradient overlay (gradient_overlay) and mockups (mockups)-->

            <!-- Main content -->
            <div class="container align-left" id="main_content">
                <div class="content_container row" >
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 front"> 
                        </div>
                        <div class="clr"></div>
                        <!-- CTA Buttons-->
                        <div class="cta_button_area">

                            <div>
                                <h3 class="opacity--1 w200">Beauty and Wellness at your fingertips</h3>
                                <!-- <div>
                                    <p class=" slogan">Look and feel fabulous with our pre-verified and trained professional stylist anywhere in your city.</p>
                                </div> -->
                             
                            
                                <button class="btn waves-effect waves-light subscribe-submit">Download Now!</button>
                                <form class="newForm" action="http://request.charmeapp.com" method="get" target="_blank">
                                    <button class="btn btn-blue waves-effect waves-light subscribe-submit">Login</button>
                                </form>
                            </div>
                        </div>
                        <!-- //CTA Buttons-->
                                    
                    </div>
                    
                </div>          
            </div> 
            <!-- //Main content -->
            
            
            <!-- Slider Background -->
             <div id="maximage_slider" class="mc-cycle">
                    <div class="mc-image">
                            <img src="{{URL::asset('images/bg.jpg')}}" alt=""/>
                    </div>
            </div>
            <!-- //Slider Background -->
                        
        </section><!-- //Homepage -->
        

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>       <!-- jQuery -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
 
  </body>
</html>
