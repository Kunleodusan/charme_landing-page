<!DOCTYPE html>
<html>
  <head>
    <title>Charmé</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>  
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    
    
    <!-- Included Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lora:400,900,700,500,300,100' rel='stylesheet' type='text/css'>
    
    
    <!-- CSS Files comes here -->
    <link href="css/main.css" rel="stylesheet" media="screen">                                   <!-- Main CSS file -->
    <link href="css/colors/color_palette_charme.css" rel="stylesheet" media="screen">              <!-- Color palette -->
    <link href="css/rapid-icons.css" rel="stylesheet" media="screen">                            <!-- James Framework - ICONS -->
    <link href="css/animate.css" rel="stylesheet" media="screen">                                <!-- Animate - animations -->
    <link href="css/nivo_lightbox_themes/default/default.css" rel="stylesheet" media="screen">   <!-- Lightbox Styles -->
    <link href="css/owl.theme.css" rel="stylesheet" media="screen">                              <!-- Owl - Carousel -->
    
    
    <!-- Modernizer and IE specyfic files -->  
    <script src="js/modernizr.custom.js"></script>  
       
  </head>
  
  <body>
  
    <!-- Mobile nav -->      
    <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
        <div class="mobile_nav_close_button"><a href="" id="hideRight"><div data-icon="&#xe13f;" class="button_icon close_icon"></div></a></div>
        <nav id="mobile_menu_content">
            <a href="#more_info" >Features</a>
            <a href="#features" >Gift Cards</a>
            <a href="#features_video" >Weddings</a>
            <a href="#reviews" >About</a>
            <a href="#screenshots" >Charme Pro</a>
            <a href="#contact" >Contact</a>
        </nav>
    </div>
    <!-- //Mobile nav -->
  
    <div id="preloader_container">
        
        <!-- Preloader Screen -->
        <header class="preloader_header">    
            <div class="preloader_loader">
                <svg class="preloader_inner" width="60px" height="60px" viewBox="0 0 80 80">
                    <path class="preloader_loader_circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id="preloader_loader_circle" class="preloader_loader_circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </header>
        <!-- //Preloader Screen -->
    
    
        <!-- Fullscreen homepage -->
        <section class="hero_fullscreen background_slider menu_bar-waypoint" data-animate-down="menu_bar-hide" data-animate-up="menu_bar-hide" >
            <!-- This section class is where you can edit background choice (background_single, background_slider, background_video) you can also enable gradient overlay (gradient_overlay) and mockups (mockups)-->
        

            <!-- Logo and navigation -->
            <div class="container top_bar" >
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <nav class="navigation_desktop">
                        <ul>
                            <li><a href="#more_info" class="visible-lg visible-md go_to_overview">Features</a></li>
                            <li><a href="#features" class="visible-lg visible-md">Gift Cards</a></li>
                            <li><a href="#features_video" class="visible-lg visible-md">Weddings</a></li>
                            <li><a href="#reviews" class="visible-lg visible-md">About</a></li>
                            <li><a href="#screenshots" class="visible-lg visible-md">Charme Pro</a></li>
                            <li><a href="#contact" class="visible-lg visible-md">Contact</a></li>
                            <li><div class="mobile_nav_open_button hidden-lg hidden-md"  ><a href="" id="showRight_1" ><div data-icon="&#xe2f3;" class="button_icon close_icon"></div></a></div></li>
                        </ul>
                    </nav>
                    </div>
                </div>
            </div>
            <!-- //Logo and navigation -->


            <!-- Main content -->
            <div class="container align-left" id="main_content">
                <div class="content_container row" >
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 home_content">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 front">  
                            <img src="{{URL::asset('images/bigCharme.png')}}"  alt="logo" class="front" />
                            <sub class="float-right">“Professional Beauty and Wellness service”</sub>
                        </div>
                        <div class="clr"></div>
                        <!-- CTA Buttons-->
                        <div class="cta_button_area">

                            <div>
                                <h3 class="opacity--1 w200">Beauty and Wellness at your fingertips</h3>
                                <div>
                                <p class=" slogan">Look and feel fabulous with our pre-verified and trained professional stylist anywhere in your city.</p>
                                </div>
                             
                            
                                <button class="btn waves-effect waves-light subscribe-submit">Download Now!</button>
                                <form class="newForm" action="http://request.charmeapp.com" method="get" target="_blank">
                                    <button class="btn btn-blue waves-effect waves-light subscribe-submit">Login</button>
                                </form>
                            </div>
                        </div>
                        <!-- //CTA Buttons-->
                                    
                    </div>
                    
                </div>          
            </div> 
            <!-- //Main content -->
            
            
            <!-- Slider Background -->
     <div id="maximage_slider" class="mc-cycle">
            <div class="mc-image">
                    <img src="{{URL::asset('images/bg.jpg')}}" alt=""/>
                    <div class="in-slide-content">


                                    
                    </div>
            </div>


             <div class="mc-image">
                <img src="{{URL::asset('images/bg2.jpg')}}" alt=""/>
                <div class="in-slide-content">



                </div>
            </div>
    </div>
            <!-- //Slider Background -->
                        
        </section><!-- //Homepage -->
        
        
        <!-- Menu bar -->
        <header id="menu_bar" class="menu_bar">
            <div class="container">
                <a href="" class="go_to_home logo_dark"><img src="{{URL::asset('images/charme_pink.png')}}" alt="logo" class="logo"/></a>
                <nav class="menu_bar_navigation">
                    <ul>
                        <li class="visible-lg visible-md"><a href="#more_info" class="go_to_overview" >Features</a></li>
                        <li class="visible-lg visible-md"><a href="#features" >Gift Cards</a></li>
                        <li class="visible-lg visible-md"><a href="#features_video" >Weddings</a></li>
                        <li class="visible-lg visible-md"><a href="#reviews" >About</a></li>
                        <li class="visible-lg visible-md"><a href="#screenshots" >Charme Pro</a></li>
                        <li class="visible-lg visible-md"><a href="#contact" >Contact</a></li>
                        <li class="visible-lg visible-md last-item">                            
                            <form class="newForm" action="http://request.charmeapp.com" target="_blank" method="get">
                                <button class="btn waves-effect waves-light subscribe-submit">Login</button>
                                </form>
                        </li>
                        <li class="hidden-lg hidden-md"><div class="mobile_nav_open_button "><a href="" id="showRight_2"><div data-icon="&#xe2f3;" class="button_icon close_icon"></div></a></div></li>
                    </ul>
                </nav>
            </div>
        </header><!-- // Menu bar -->
    
        
        <!-- More Info #################### -->
        
        <section id="more_info" data-animate-down="menu_bar-show" data-animate-up="menu_bar-hide" class="menu_bar-waypoint subsection">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="">
                            <img src="{{URL::asset('images/view_app.png')}}" alt="" class="img_responsive mockup_back content_anim1" />
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    
                        <div class="padding-botom--0px intro content_anim2">
                            <div data-icon="&#xe036;" class="iconbox_icon pink"></div>
                            <div class="iconbox_content">
                                <h5>Charme is the best way to get grooming professionals.</h5>
                                <p>Top professions for Makeup, Hairdressing, Fitness, In-home  Spa, Barbing & Errands.</p>
                            </div>
                        </div>
                        <div class="iconbox content_anim4">
                            <div data-icon="&#xe0f3;" class="iconbox_icon pink"></div>
                            <div class="iconbox_content">
                                <h5>Where are we?</h5>
                                <img src="{{URL::asset('images/sity1.png')}}" style="width: 30%;">
                                <img src="{{URL::asset('images/sity3.png')}}" style="width: 30%;" class="content_anim5">
                            </div>
                        </div>
                            
                    </div>
                </div>              
            </div>
            
            
        </section><!-- //More info -->
        
        
        <!-- Features #################### -->
        
        <section id="features" class="subsection bg-white">
            <div class="container">
                <div class="row">
                
                    
                    
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    
                        <div class="content_anim6">
                            <h3>Gift Cards</h3>
                            <p class="content_anim7">Make that special person feel loved.</p>
                        </div>
                        
                        <div class="iconbox content_anim8">
                            <img src="{{URL::asset('images/makeup.png')}}" style="width: 30%;">
                            <img src="{{URL::asset('images/hair.png')}}" style="width: 30%;">
                            <img src="{{URL::asset('images/homespa.png')}}" style="width: 30%;">
                        </div>
                        
                        <div class="iconbox content_anim9">
                            <div>
                                    <button class="btn waves-effect waves-light subscribe-submit">Download Now!</button>
                                    <button class="btn btn-blue waves-effect waves-light subscribe-submit" onclick="window.open('http://request.charmeapp.com')">Login</button>
                            </div>
                            <p>
                                <a>
                                    <div data-icon="&#xe036;" class="iconbox_icon pink"></div>
                                    <strong>Instantly get 10% off when you gift a service</strong>
                                </a>
                            </p>
                        </div>
                            
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div>
                            <img src="{{URL::asset('images/charme_cando33.png')}}" alt="" class="img_responsive mockup_back content_anim10" />
                        </div>
                    </div>

                </div>              
            </div>  
        </section><!-- //Features -->
        


        <!-- Features - Video #################### -->
        
        <section id="features_video" class="subsection">
            <div class="container">

                <div class="row">
                    <div  class="col-xs-12 col-sm-6 col-md-6 col-lg-6 content_anim11">
                        <div class="">
                        <img src="{{URL::asset('images/charme_cando4.png')}}" alt="" class="img_responsive mockup_back" />
                        </div>
                    </div>                  
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 content_anim12" >
                        <div class="intro" style="music_style">
                            <div>
                                <h3 class="content_anim14">Weddings</h3>
                                <div class="iconbox_content content_anim15">
                                
                                <img src="{{URL::asset('images/icons/errand.png')}}" style="width: 15%;">
                                <img src="{{URL::asset('images/icons/spa.png')}}" style="width: 15%;">
                                <img src="{{URL::asset('images/icons/fitness.png')}}" style="width: 15%;">
                                <img src="{{URL::asset('images/icons/hairdressing.png')}}" style="width: 15%;">
                                <img src="{{URL::asset('images/icons/makeup.png')}}" style="width: 15%;">
                                <img src="{{URL::asset('images/icons/haircut.png')}}" style="width: 15%;">
                            </div>
                            <div class="content_anim16">
                                <p><strong>Take the hassle out of this all important day.</strong></p>
                                <p>We offer premium services, amazing discounts and a team of stylists who will be on hand at every step making sure that you all look your best.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- //Features - Video -->
        
        
        <!-- Reviews #################### -->
        
        <section id="reviews" class="subsection"  data-stellar-background-ratio="0.6">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    
                        <div class="intro">
                            <h3 class="content_anim18">ABOUT</h3>
                            <p class="content_anim19">Charme is changing the way people access beauty and wellness services across the African continent.</p>
                            <p class="content_anim20">Charme brings Makeup, Hairdressing, Barbing, Massage and Fitness right to your doorstep, anywhere and at any time.</p>
                            <p class="content_anim21">Giving you access to pre-verified and trained professionals at the touch of a button. It has never been this easy to get top notch professional services at such prices.</p>
                        </div>              
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="content_anim17">
                            <img src="{{URL::asset('images/charme_hd.png')}}"  alt="" class="img_responsive mockup_back content_anim10" />
                        </div>
                    </div>
                    
                    
                </div>      
            </div>
        </section><!-- //Reviews -->
        
        
        <!-- Reviews - Logos #################### -->
        
        <section id="reviews_logos" class="subsection bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-center">
                        <div class="intro content_anim18">
                            <h3>Our app in newspapers!</h3>
                        </div>
                    </div>
                </div>  

                <div class="row">
                    <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 reviews_logo content_anim19"><img src="{{URL::asset('images/reviews_logos/logo1.png')}}" alt=""></div>
                    <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 reviews_logo content_anim20"><img src="{{URL::asset('images/reviews_logos/logo2.png')}}" alt="" ></div>
                    <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 reviews_logo content_anim21"><img src="{{URL::asset('images/reviews_logos/logo3.png')}}" alt="" ></div>
                    <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 reviews_logo content_anim22"><img src="{{URL::asset('images/reviews_logos/logo4.png')}}" alt="" ></div>
                    <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 reviews_logo content_anim23"><img src="{{URL::asset('images/reviews_logos/logo5.png')}}" alt="" ></div>
                    <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 reviews_logo content_anim24"><img src="{{URL::asset('images/reviews_logos/logo6.png')}}" alt="" ></div>
                </div>

            </div>
        </section><!-- //Reviews - Logos-->
        
        
        <!-- Screenshots #################### -->
        
        <section id="screenshots" class="subsection">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="">
                        <img src="{{URL::asset('images/imgs.png')}}"  alt="" class="img_responsive mockup_back content_anim25" />
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">                  
                        <div class="intro content_anim26 text--white">
                            <h3>CHARME PRO</h3>
                            <p>Charme lets you book top A-list Professionals for your weddings, events or as a special treat.</p>
                            <p>
                                Gain access to thousands of customers in your city, join Charme today. Just tell us what you do and where you do it and we will send the customers your way.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
        </section><!-- //Screenshots-->
        
        
        <!-- Newsletter #################### -->
        
        <section id="newsletter" class="subsection_parallax"  data-stellar-background-ratio="0.6">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-center content_anim27">
                        <div class="intro">
                            <h3>Sign up to Our newsletter</h3>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-center content_anim28">

                    <!-- Newsletter Form -->
                    <div class="newsletter_form">
                        <form action="php/send.php" method="post" id="subscribe-form">
                        <div class="input-field">
                            <div data-icon="&#xe563;" class="prefix"></div>
                            <input id="email" type="email" name="email" class="validate">
                            <label for="email">Email address</label>    
                        </div>
                        <button class="btn waves-effect waves-light subscribe-submit" type="submit" name="action" id="subscribe-form-submit">Subscribe Now</button>
                        </form>
                        <div id="preview"></div>
                    </div>
                    <!-- //Newsletter form -->

                    </div>
                </div>
                    
            </div>
        </section><!-- //Newsletter-->
        
        
        <section id="contact" class="subsection">
            <div class="container">
                <div class="row">
                    
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 content_anim29">
                        <h3>Contact Info</h3>
                        <p>Like to know more about us, get across to us by any means convenient to you.</p>
                        <ul class="icon_list">
                            <li><div data-icon="&#xe1b4;" class="icon_small float-left pink"></div><h6>African Hub, 23 Amore Street, Off Toyin Street<br>Ikeja, Lagos, Nigeria</h6></li>
                            <li><div data-icon="&#xe23a;" class="icon_small float-left pink"></div><h6>09050505047</h6></li>
                            <li>
                                <div data-icon="&#xe242;" class="icon_small float-left pink"></div>
                                <h6 onclick="location.href='mailto:info@charmeapp.com'">
                                    info@charmeapp.com
                                </h6>
                            </li>
                        </ul>                       
                    </div>
                    
                    
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 content_anim30">
                        <h3>Contact Us</h3>
                        <p>Contact Us using contact form below.</p>
                    
                    
                        <!-- Contact Form -->
                        <form action="{{URL::asset('php/contactform.php')}}" id="contact-form" method="post">
                            <div class="contact_form">
                                <div class="input-field">
                                    <input id="first_name" type="text" name="contact-name">
                                    <label for="first_name">Your Name</label>
                                </div>
                                <div class="input-field">
                                    <input id="contact_email" type="email" name="contact-email">
                                    <label for="contact_email">Email Address</label>
                                </div>
                                <div class="input-field">
                                    <textarea class="materialize-textarea" name="contact-message"></textarea>
                                    <label>Message</label>
                                </div>
                            </div>  
                            <button class="btn waves-effect waves-light" type="submit" name="action">Send</button>
                        </form>
                        <!-- //Contact Form -->
                    
                    
                        <div id="message"><div id="alert"></div></div><!-- Message container --> 
                    </div>
                    
                    

                </div>              
            </div>
        </section><!-- //More info -->
        
        
        <section id="footer" class="subsection bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-center">
                    
                        <!-- Social Icons -->
                        <div class="social_icons_container align-center">
                            <div class="social_icons">
                                <ul>
                                    <li><div data-icon="&#xe282;" class="social_icon twitter_icon" onclick="location.href='twitter.com'"></div></li>
                                    <li><div data-icon="&#xe281;" class="social_icon facebook_icon" onclick="location.href='facebook.com'"></div></li>
                                    <li><div data-icon="&#xe279;" class="social_icon linkdin_icon" onclick="location.href='linkedin.com'"></div></li>
                                </ul>
                            </div>                          
                        </div>  
                        <!-- //Social Icons -->
                        <p class="policy">
                            <a href="{{URL::asset('PrivacyPolicy.html')}}">Privacy Policy</a> |
                            <a href="{{URL::asset('serviceA.html')}}">Service Agreement</a> |
                            <a href="{{URL::asset('Terms_conditions.html')}}">Terms & Condition</a>
                        </p>
                        <p>
                            <small>Copyright © 2016 Charme, All rights reserved.</small>
                        </p>    
                    </div>
                </div>              
            </div>
        </section><!-- //More info -->
        

    </div><!-- //preloader -->
    
   
    <!-- JavaScript plugins comes here -->  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>       <!-- jQuery -->
    
    <script src="{{URL::asset('js/output2.min.js')}}"></script>       <!-- Combined required -->  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>               <!-- Materialize -->
   <script src="{{URL::asset('js/output.min.js')}}"></script>                    <!-- COmbined Js file -->
    
    
    <!-- Show message after newsletter form submit -->
    <script type="text/javascript">
        $('document').ready(function()
            {
            $('#subscribe-form').ajaxForm( {
                target: '#preview',
                success: function() {       
                      $('#preview').addClass( "show_message" );
                    }
                });
            $('.policy > a').nivoLightbox();
            });
    </script>


    <!-- MOBILE NAVIGATION -->
    <script>
        var showRight_1 = document.getElementById( "showRight_1" ),
        showRight_2 = document.getElementById( "showRight_2" ),
        hideRight = document.getElementById( 'hideRight' ),
        menuRight = document.getElementById( 'cbp-spmenu-s2' ),
        body = document.body;
        
        showRight_1.onclick = function(event) {
            "use strict";
            event.preventDefault();
            classie.toggle( this, 'active' );
            classie.toggle( menuRight, 'cbp-spmenu-open' );
            disableOther( 'showRight_1' );
            return false;
        };
        
        showRight_2.onclick = function(event) {
            "use strict";
            event.preventDefault();
            classie.toggle( this, 'active' );
            classie.toggle( menuRight, 'cbp-spmenu-open' );
            disableOther( 'showRight_2' );
            return false;
        };
    
        hideRight.onclick = function(event) {
            "use strict";
            event.preventDefault();
            classie.toggle( this, 'active' );
            classie.toggle( menuRight, 'cbp-spmenu-open' );
            disableOther( 'showRight_2' );
            return false;
        };
    </script>
    
    
    <!-- Nav Bar show/hide after scrolling -->
    <script>
    var $head = $( '#menu_bar' );
    $( '.menu_bar-waypoint' ).each( function(i) {
        var $el = $( this ),
            animClassDown = $el.data( 'animateDown' ),
            animClassUp = $el.data( 'animateUp' );
            

        $el.waypoint( function( direction ) {
            if( direction === 'down' && animClassDown ) {
                $head.attr('class', 'menu_bar ' + animClassDown);
            }
            else if( direction === 'up' && animClassUp ){
                $head.attr('class', 'menu_bar ' + animClassUp);
            }
        }, { offset: function() {
            navbarheight = $("#menu_bar").outerHeight()+1;
            return navbarheight;
            }
         } );
    } );
    </script>
    
  </body>
</html>