<!DOCTYPE html>
<html>
  <head>
    <title>Charmé - Request Service</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>  
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/materialize.clockpicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/order.css')}}">
  </head>

  <body>
    <nav>
      <div class="nav-wrapper">
        <a href="{{url('/')}}" class="brand-logo"><img src="{{URL::asset('images/charme_pink.png')}}" alt="logo" class="logo"/></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <!-- <li><a href="sass.html">Sass</a></li> -->
        </ul>
      </div>
    </nav>
    <br/>

    @if(isset($success))
      <div class="row">
        <div class="col s12 m6 offset-m3">
          <div class="card green darken-1">
            <div class="card-content white-text">
              <span class="card-title">Request Successful.</span>
              <p>
                <ul>
                  {!!$success!!}
                </ul>
              </p>
            </div>
          </div>
        </div>
      </div>
    @endif

    @if(count($errors) > 0)
      <div class="row">
        <div class="col s12 m6 offset-m3">
          <div class="card red darken-1">
            <div class="card-content white-text">
              <span class="card-title">We encountered some errors while processing your request. Please bear with us.</span>
              <p>
                <ul>
                  @foreach($errors->all() as $error)
                    <li>- <i>{{$error}}</i></li>              
                  @endforeach
                </ul>
              </p>
            </div>
          </div>
        </div>
      </div>
    @endif

    <!-- Card for service form -->
    <div class="row">
        <div class="col s12 m6 offset-m3">
          <div class="card darken-1">
            <div class="card-content black-text">
              <div class="card-title form-title charme-color"><strong>Order Service Form</strong></div>
              <div class="row">
                <form class="col s12" method="post" action="{{url('/order')}}">
                  <div class="row">
                    <div class="input-field col s12 m6">
                      <input name="first_name" id="first_name" type="text" class="validate" required>
                      <label for="first_name">First Name</label>
                    </div>
                    <div class="input-field col s12 m6">
                      <input name="last_name" id="last_name" type="text" class="validate" required>
                      <label for="last_name">Last Name</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12 m6">
                      <input name="email" id="email" type="email" class="validate" required>
                      <label for="email">Email</label>
                    </div>
                    <div class="input-field col s12 m6">
                      <input name="phone_no" id="phone_no" type="tel" class="validate" required>
                      <label for="phone_no">Phone Number</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <input name="house_address" id="address" type="text" class="validate" required>
                      <label for="address">House Address</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <input name="nearest_bus_stop" id="bus_stop" type="text" class="validate" required>
                      <label for="bus_stop">Nearest Bus Stop to your house</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <input name="service_name" id="service" type="text" class="validate" required>
                      <label for="service">What service do you need</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col s6">
                      Appointment Date: <input name="appointment_date" type="date" class="datepicker" required>
                    </div>
                    <div class="col s6">
                      Appointment Time: <input name="appointment_time" type="time" class="timepicker" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col s6">
                      How should we reach you:
                    </div>
                    <div class="col s6">
                      <input name="contact_via" type="radio" id="via_mail" value="Mail" checked="checked"/>
                      <label for="via_mail">Mail</label>
                      &nbsp;&nbsp;
                      <input name="contact_via" type="radio" id="via_phone" value="Phone"/>
                      <label for="via_phone">Phone</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <textarea name="additional_info" id="textarea1" class="materialize-textarea"></textarea>
                      <label for="textarea1">Do you want any additional thing to be brought to perform the service?</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <textarea name="additional_address_info" id="textarea2" class="materialize-textarea"></textarea>
                      <label for="textarea2">Any other details to help us get to your house:</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <textarea name="additional_request_info" id="textarea3" class="materialize-textarea"></textarea>
                      <label for="textarea3">Explain in details what other thing you want to be done:</label>
                    </div>
                  </div>
                  <div class="row">
                    <button class="btn waves-effect waves-light col s12 charme-bg-color" type="submit">
                        Submit
                    </button>
                  </div>
                  
                  <div class="card-action">
                    <span class="charme-color"><strong><em>
                      **We will call to confirm your order soon. Please you will be required to pay before the service 
                      provider comes to your house.**</em></strong>
                    </span>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
    <script type="text/javascript" src="{{asset('js/materialize.clockpicker.js')}}"></script>
    <script type="text/javascript">
      $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year
        format: 'yyyy-mm-dd',
      });

      $('.timepicker').clockpicker({
        twelvehour: true,
        afterDone: function() {
          //convert 12 hours time to 24 hours for submition
          var time = $('.timepicker').val();
          var hours = Number(time.match(/^(\d+)/)[1]);
          var minutes = Number(time.match(/:(\d+)/)[1]);
          var AMPM = (time.indexOf('AM') != -1) ? 'AM' : 'PM';
          if(AMPM == "PM" && hours<12) hours = hours+12;
          if(AMPM == "AM" && hours==12) hours = hours-12;
          var sHours = hours.toString();
          var sMinutes = minutes.toString();
          if(hours<10) sHours = "0" + sHours;
          if(minutes<10) sMinutes = "0" + sMinutes;
          
          var hoursFormat = sHours + ":" + sMinutes;
          $('.timepicker').val(hoursFormat);
        }
      });
    </script>
  </body>
</html>