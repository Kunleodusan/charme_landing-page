<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InstantOrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone_no' => 'required',
            'house_address' => 'required',
            'nearest_bus_stop' => 'string',
            'service_name' => 'required',
            'appointment_date' => 'required|date_format:Y-m-d',
            'appointment_time' => 'required|date_format:H:i',
            'contact_via' => 'required|in:Mail,Phone',
            'additional_info' => 'string',
            'additional_address_info' => 'string',
            'additional_request_info' => 'string',
        ];
    }
}
