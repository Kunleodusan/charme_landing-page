<?php
use App\Http\Requests\InstantOrderRequest;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/2', function () {
    return view('home');
});
Route::get('/','HomeController@viewHome');

Route::get('/pricing', function () {
    return view('prices');
});

Route::get('/order', ['middleware' => 'web', function (Request $request) {
	return view('orderService');
}]);

Route::post('order', ['middleware' => 'web', function (InstantOrderRequest $request) {
	$acceptedFields = [
		'first_name', 'last_name', 'email', 'phone_no', 'house_address', 'nearest_bus_stop',
		'service_name', 'appointment_date', 'appointment_time', 'contact_via', 'additional_info',
		'additional_address_info', 'additional_request_info',
	];

	$uri=getenv("CHARME_API");
	$response = \Httpful\Request::post($uri."instant-requests")
					->body($request->only($acceptedFields))->sendsType(\Httpful\Mime::FORM)
					->expectsJson()->send();
    $response= json_decode((string) $response,true);
	
    if($response['status'] == 'ok')
    {
    	//all went well
    	return view('orderService', [
    		'success' => 'Your request has been received. You will be contacted by a Charme Operative soon.<br/>Thank you for using Charme!'
		]);
    }
    else
    {
    	//error occurred
    	return view('orderService')->withErrors($response['error']['msg']);
    }
}]);

Route::get('/review', function () {
    return view('homeReview');
});
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
