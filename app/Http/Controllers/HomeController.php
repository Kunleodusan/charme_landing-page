<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class HomeController extends Controller
{
    public function viewHome() {
        $uri=getenv("CHARME_API");
    	$response = \Httpful\Request::get($uri."categories")->expectsJson()->send();
        $response= json_decode((string) $response,true);
	    if ($response['status']=='ok') {
	    	return view('homeVideo', ['categories'=>$response['data']]);
		} else return view('homeVideo', ['categories' => []]);
    }
}
